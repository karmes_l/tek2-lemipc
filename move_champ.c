/*
** move_champ.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Concurrence/PSU_2015_lemipc/test
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Mar 22 17:54:30 2016 Karmes Lionel
** Last update Sun Mar 27 19:24:50 2016 Karmes Lionel
*/

#include "lemipc_function.h"

int		is_stuck(struct s_case *map, struct s_case *champ,  int team_nb)
{
  int		pos;
  int		tab[8];

  pos = champ->x + (champ->y * WIDTH);
  tab[0] = (pos % WIDTH == WIDTH - 1 ? -1 : pos + 1);
  tab[1] = (pos % WIDTH == 0 ? -1 : pos - 1);
  tab[2] = (pos / WIDTH == HEIGHT - 1 ? -1 : pos + WIDTH);
  tab[3] = (pos / WIDTH == 0 ? -1 : pos - WIDTH);
  tab[4] = (tab[2] == -1 || tab[1] == -1 ? -1 : pos + (WIDTH - 1));
  tab[5] = (tab[2] == -1 || tab[0] == -1 ? -1 : pos + (WIDTH + 1));
  tab[6] = (tab[3] == -1 || tab[1] == -1 ? -1 : pos - (WIDTH - 1));
  tab[7] = (tab[3] == -1 || tab[0] == -1 ? -1 : pos - (WIDTH + 1));
  if (two_ennemies(map, tab, team_nb))
    return (1);
  return 0;
}

struct s_case*	move_champ(struct s_case *map, struct s_case *champ, int team_nb)
{
  int		pos;
  int		i;
  int		tb[4];

  i = rand() % 4;
  pos = champ->x + (champ->y * WIDTH);   
  tb[0] = (pos % WIDTH == WIDTH - 1 ? -1 : pos + 1);
  tb[1] = (pos % WIDTH == 0 ? -1 : pos - 1);
  tb[2] = (pos / WIDTH == HEIGHT - 1 ? -1 : pos + WIDTH);
  tb[3] = (pos / WIDTH == 0 ? -1 : pos - WIDTH);
  while (tb[i] == -1 || map[tb[i]].nteam != 0)
    i = rand() % 4;
  map[pos].sops.sem_op = 1;  
  semop(map[pos].sem_id, &map[pos].sops, 1); 
  map[tb[i]].sops.sem_op = -1;
  semop(map[tb[i]].sem_id, &map[tb[i]].sops, 1);
  champ = &map[tb[i]];
  map[pos].nteam = 0;
  champ->nteam = team_nb;
  /* printf("x = %d \ny = %d\n", map[pos].x, map[pos].y); */
  /* printf("x = %d \ny = %d\n", champ->x, champ->y); */
  return (champ);

}

void		del_ressource(int shmid, struct s_case *map, int flag_champ)
{
  int		x;

  x = 0;
  while (x < WIDTH * HEIGHT)
    {
      semctl(map[x].sem_id, 0, IPC_RMID);
      ++x;
    }
  if (flag_champ == 1)
    shmctl(shmid, IPC_RMID, NULL);
}

int		first_champ(int shmid, int team_nb)
{
  struct s_case	*map;
  struct s_case	*champ;
  int		alive;

  if (!(champ = create_champ(shmid, team_nb, &map)))
    return (0);
  alive = 1;
  print_map(shmid);
  read_signal();
  while (1)
    {
      if (g_signal_int)
	break ;
      if (alive && is_stuck(map, champ, team_nb))
	{
	  champ->nteam = 0;
	  alive = 0;
	}
      sleep(1);
      if (alive)
        champ = move_champ(map, champ, team_nb);
      print_map(shmid);
    }
  del_ressource(shmid, map, 1);
  return (1);
}

int	not_first_champ(int shmid, int team_nb)
{
  struct s_case	*map;
  struct s_case	*champ;

  if (!(champ = create_champ(shmid, team_nb, &map)))
    return (0);
  if ((map = shmat(shmid, NULL, SHM_R | SHM_W)) == (void *) -1)
    return (fprintf(stderr, "Error shmat : %s\n", strerror(errno)), 0);
  while (is_stuck(map, champ, team_nb) != 1)
    {
      sleep(1);
        champ = move_champ(map, champ, team_nb);
    }
  champ->nteam = 0;
  del_ressource(shmid, map, 0);
  return (1);
}
