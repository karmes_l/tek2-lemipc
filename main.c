/*
** main.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Concurrence/PSU_2015_lemipc/test
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Mar 22 10:42:12 2016 Karmes Lionel
** Last update Sun Mar 27 18:39:43 2016 Karmes Lionel
*/

#include "lemipc_function.h"

sig_atomic_t	g_signal_int = 0;

int		print_map(int shmid)
{
  int		x;
  void		*addr;
  struct s_case	*case_map;

  if ((addr = shmat(shmid, NULL, SHM_R | SHM_W)) == (void *) -1)
    return (fprintf(stderr, "Error shmat : %s\n", strerror(errno)), 0);
  case_map = addr;
  x = 0;
  while (x < WIDTH * HEIGHT)
    {
      if (case_map[x].x == 0 && x != 0)
	printf("\n");
      if (case_map[x].nteam == 0)
	printf(" ");
      else
	printf("%d", case_map[x].nteam);
      ++x;
    }
  printf("\n");
  return (1);
}

int		init_map(void *addr, key_t key)
{
  int		x;
  struct s_case	*case_map;
  int		sem_id;
  struct sembuf	*sops;

  case_map = addr;
  x = 0;
  if ((sops = malloc (sizeof(struct sembuf) * (HEIGHT * WIDTH))) == NULL)
    return (fprintf(stderr, "Malloc error\n"));
  sem_id = semget(key, HEIGHT * WIDTH, IPC_CREAT | SHM_R | SHM_W);  
  while (x < WIDTH * HEIGHT)
    {
      sops[x].sem_num = x;
      sops[x].sem_flg = IPC_NOWAIT;
      sops[x].sem_op = -1;
      case_map[x].x = x % WIDTH;
      case_map[x].y = x / WIDTH;
      case_map[x].nteam = 0;
      case_map[x].sem_id = sem_id;
      case_map[x].sops = sops[x];
      ++x;
    }
  return (1);
}

int	createMap(key_t key, int *shmid)
{
  void	*addr;

  if ((*shmid = shmget(key, WIDTH * HEIGHT * sizeof(struct s_case),
		      IPC_CREAT | SHM_R | SHM_W)) == -1)
    return (fprintf(stderr, "Error shmget : %s\n", strerror(errno)), 0);
  if ((addr = shmat(*shmid, NULL, SHM_R | SHM_W)) == (void *) -1)
    return (fprintf(stderr, "Error shmat : %s\n", strerror(errno)), 0);
  init_map(addr, key);
  return (1);
}

int		lemipc(char *path, int team_nb)
{
  int		shmid;
  key_t	key;

  if ((key = ftok(path, 1)) == -1)
    return (fprintf(stderr, "Error ftok : %s\n", strerror(errno)), 0);
  if ((shmid = shmget(key, WIDTH * HEIGHT * sizeof(struct s_case),
		      SHM_R | SHM_W)) == -1 &&
      errno != ENOENT)
    return (fprintf(stderr, "Error shmget : %s\n", strerror(errno)), 0);
  if (shmid == -1)
    {
      if (!createMap(key, &shmid))
	return (0);
      if (!first_champ(shmid, team_nb))
	return (0);
    }
  else
    if (!not_first_champ(shmid, team_nb))
      return (0);
  return (1);
}

int	main(int ac, char **av)
{
  int	team_nb;

  if (ac < 3)
    return (fprintf(stderr, "Usage : %s PATH TEAM_NUMBER\n", av[0]), 1);
  if ((team_nb = atoi(av[2])) == 0)
    return (fprintf(stderr, "Invalid team number\n"), 1);
  if (!lemipc(av[1], team_nb))
    return (1);
  return (0);
}
