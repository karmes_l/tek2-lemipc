/*
** create_champ.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Concurrence/PSU_2015_lemipc
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Sun Mar 27 19:13:51 2016 Karmes Lionel
** Last update Sun Mar 27 19:18:04 2016 Karmes Lionel
*/

#include "lemipc_function.h"

struct s_case*	add_champion(struct s_case *map, int team_nb)
{
  struct s_case	*champ;
  int		i;
  int		j;


  i = 0;
  j = 0;
  srand(time(NULL));
  while (map[i].nteam != 0 && j++ < WIDTH * HEIGHT)
    i = rand() % (WIDTH * HEIGHT);
  champ = &map[i];
  champ->nteam = team_nb;
  return (champ);
}

struct s_case*	create_champ(int shmid, int team_nb, struct s_case **map)
{
  if ((*map = shmat(shmid, NULL, SHM_R | SHM_W)) == (void *) -1)
    return (fprintf(stderr, "Error shmat : %s\n", strerror(errno)), NULL);
  return (add_champion(*map, team_nb));
}
