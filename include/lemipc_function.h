/*
** lemipc.h for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Concurrence/PSU_2015_lemipc/test/include
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Mar 22 10:49:12 2016 Karmes Lionel
** Last update Sun Mar 27 19:17:46 2016 Karmes Lionel
*/

#ifndef LEMIPC_FUNCTION_H_
# define LEMIPC_FUNCTION_H_

# include <sys/ipc.h>
# include <sys/types.h>
# include <sys/shm.h>
# include <sys/sem.h>
# include <stdio.h>
# include <stdlib.h>
# include <errno.h>
# include <string.h>
# include <time.h>
# include <unistd.h>
# include <signal.h>
# include "lemipc.h"

# define WIDTH 10
# define HEIGHT 10

extern sig_atomic_t	g_signal_int;

int	print_map(int shmid);
struct s_case*	add_champion(struct s_case *map, int teamnb);
int	first_champ(int shmid, int teamnb);
int	not_first_champ(int shmid, int teamnb);
int	two_ennemies(struct s_case *map, int *vision, int team_nb_self);
void	read_signal();
struct s_case*	create_champ(int shmid, int team_nb, struct s_case **map);

#endif /* !LEMIPC_FUNCTION_H_ */
