/*
** read_signal.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Concurrence/PSU_2015_lemipc
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Sun Mar 27 16:49:07 2016 Karmes Lionel
** Last update Sun Mar 27 16:50:34 2016 Karmes Lionel
*/

#include "lemipc_function.h"

void    get_sigint(int sign)
{
  g_signal_int = sign;
}

void	read_signal()
{
  g_signal_int = 0;
  if (signal(SIGINT, get_sigint) == SIG_ERR)
    fprintf(stderr, "[ERROR] : SIGQUIT\n");
}
