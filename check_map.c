/*
** check_map.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Concurrence/PSU_2015_lemipc/test
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Sun Mar 27 13:00:24 2016 Karmes Lionel
** Last update Sun Mar 27 16:22:19 2016 Karmes Lionel
*/

#include "lemipc_function.h"

int		count_champ(struct s_case *map, int *vision, int champ)
{
  int		i;
  int		c;

  i = 0;
  c = 0;
  while (i < 8)
    {
      if (vision[i] != -1 && map[vision[i]].nteam == champ)
	++c;
      ++i;
    }
  return (c);
}

int	        two_ennemies(struct s_case *map, int *vision, int team_nb_self)
{
  int		i;

  i = 0;
  while (i < 8)
    {
      if (vision[i] != -1 &&
	  map[vision[i]].nteam != team_nb_self && map[vision[i]].nteam != 0 &&
	  count_champ(map, vision, map[vision[i]].nteam) == 2)
	return (1);
      ++i;
    }
  return (0);
}
